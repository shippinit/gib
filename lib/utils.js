export const makeLinkText = (fetchedData, defaultText) => {
  return `${fetchedData || defaultText}`
}

export const craftLinkHref = (href) => {
  const link = href?.toString().replace(/\s+/g, '-')
  return `${link?.toLowerCase() || '#'}`
}