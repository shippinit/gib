# Next React Boiler

## Current Tasks
- [x] Configure Linter
- [ ] Refine folder structure
- [x] Turn on preview mode
- [x] Set up & configured Bitbucket
- [x] Configure Vercel Deploys
- [x] Set up Jest & Configure code coverage
- [x] Configure TS
- [ ] Convert project pieces to TS
- [ ] Define and implement caching strategy
- [ ] Build basic Docker Compose config for services
- [ ] Optimize bundles/Defer heavy JS where possible
- [ ] Install & configure logging
- [ ] Configure HTTP error pages
- [ ] Performance measuring
- [ ] Determine auth strategy
- [ ] Configure Cypress for CI
